﻿module obs.reactor

open System
open obs.model

module constants =
    let [<Literal>] max = 5
    let [<Literal>] min = -5

/// The state transition function. 
/// It takes a current state and an event and returns the next state
let stateTransition 
        (currentState:State) 
        (userInput:UserInput)
        : State = 
    match currentState with
    | Value currentValue ->
        match userInput with
        | Add -> 
            let newValue = currentValue + 1
            if newValue > constants.max 
            then TooBig
            else Value newValue
        | Subtract -> 
            let newValue = currentValue - 1
            if newValue < constants.min
            then TooSmall
            else Value newValue

    | TooBig ->
        match userInput with
        | Subtract -> Value constants.max
        | Add -> currentState

    | TooSmall ->
        match userInput with
        | Subtract -> currentState
        | Add -> Value constants.min 
    
/// Takes an initialState as parameter.
/// Returns a function which transforms an event source into a state source 
let main 
        (initialState:State) 
        : (UserInput IObservable -> State IObservable) = 
    Observable.scan stateTransition initialState