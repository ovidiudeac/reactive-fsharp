﻿open System

open obs
open obs.model
        
[<STAThread>]
[<EntryPoint>]
let main argv = 
    let initialState = Value 0

    let reactor = reactor.main initialState 

    let view = new view.MyForm(initialState, reactor)

    view.Show()

    0 // return an integer exit code
