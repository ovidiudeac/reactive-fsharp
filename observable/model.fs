﻿namespace obs.model

type UserInput = Add | Subtract

type State = Value of int | TooBig | TooSmall