﻿namespace obs.view

open System
open System.Windows.Forms
open FSharpx

open obs.model

/// The UI needs  an initial state (to display before any event is triggered)
/// and an event processor.
///
/// The UI has an event source which are the user's button clicks 
/// and it needs to display the state of the application
/// The eventProcessor takes an event source and returns a state source
type MyForm(initialState:State, 
            reactor:(UserInput IObservable -> State IObservable))= 
    let form = new Form(Text = "My form")
    
    let lblOutput = new Label(Dock=DockStyle.Right)
    let btnAdd = new Button(Text="Add", Dock=DockStyle.Top)
    let btnSubtract = new Button(Text="Subtract",Dock=DockStyle.Top)

    /// Displays the given state by adjusting the label text and the button states 
    let displayState state = 
        match state with
        | Value v -> 
            lblOutput.Text <- v.ToString()
            btnAdd.Enabled <- true
            btnSubtract.Enabled <- true

        | TooBig -> 
            lblOutput.Text <- "Too big"
            btnAdd.Enabled <- false
            btnSubtract.Enabled <- true

        | TooSmall -> 
            lblOutput.Text <- "Too small"
            btnAdd.Enabled <- true
            btnSubtract.Enabled <- false
        
    /// The state updates will trigger this
    let stateUpdateEvents = 
        let addEvents = btnAdd.Click |> Observable.map (konst Add)
        let subtractEvents =  btnSubtract.Click |> Observable.map (konst Subtract)
        let allEvents = Observable.merge addEvents subtractEvents
        reactor allEvents 
    
    do 
        form.Controls.AddRange 
            [|
                btnAdd
                btnSubtract
                new Label(Text="Current value: ", Dock=DockStyle.Right)
                lblOutput
            |]
    
        displayState initialState
        
        stateUpdateEvents.Add displayState 

    /// Displays the form and runs the application
    member this.Show() =
        Application.EnableVisualStyles();
        Application.Run(form)

        