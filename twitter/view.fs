﻿module view

open System
open System.Windows.Forms
open FSharp.Reactive
open TweetinCore.Interfaces
open FSharpx
open tools
open model
open agent

/// The UI needs  an initial state (to display before any event is triggered)
/// and an event processor.
///
/// The UI has an event source which are the user's button clicks 
/// and it needs to display the state of the application
/// The reactor takes an event source and returns a state source
type MyForm(initialSettings:Settings, 
            reactor:(UserInput IObservable -> State IObservable))= 
    let listRefreshPeriod = TimeSpan.FromSeconds(1.0)
    let textBoxUpdateSpan = TimeSpan.FromSeconds(0.5)
    
    let form = new Form(Text = "My form", Width=1050, Height = 700)
    
    let listboxOutput = new ListBox(Dock=DockStyle.Right, Width=800)
    let txtListSize = new TextBox(Multiline=false, Dock=DockStyle.Left)
    let txtFilter = new TextBox(Multiline=false, Dock=DockStyle.Left)
    
    let displayState (state:State) =
        let displayTweets (tweets:ITweet list) = 
            listboxOutput.BeginUpdate()
            listboxOutput.Items.Clear()
            tweets
            |> Seq.map (fun t -> tweetToStr t :> obj)
            |> Array.ofSeq
            |> listboxOutput.Items.AddRange
            listboxOutput.EndUpdate()
        
        do displayTweets state.tweets
        
    let readListSize () = Int32.parse txtListSize.Text 
    let readFilter () = 
        txtFilter.Text 
        |> Strings.split ' ' 
        |> Seq.filter (Strings.isNullOrEmpty >> not)
        |> List.ofSeq

    let stateUpdateEvents = 
        let listSizeChangedEvents = 
            txtListSize.TextChanged 
            |> Observable.throttle textBoxUpdateSpan
            |> Observable.choose (ignore >> readListSize)
            |> Observable.map ListSizeChanged

        let filterChangedEvents =  
            txtFilter.TextChanged
            |> Observable.throttle textBoxUpdateSpan
            |> Observable.map (ignore >> readFilter)
            |> Observable.map FilterChanged

        let allUiEvents = Observable.merge listSizeChangedEvents filterChangedEvents

        let agent = new ObservableAgent<UserInput>()
        allUiEvents |> Observable.add agent.Post
        
        reactor agent.MessageReceived 
        |> Observable.sample listRefreshPeriod

    let formLayout () =
        let panelListSize = new Panel(Dock = DockStyle.Top, Height=20)
        let panelFilter = new Panel(Dock = DockStyle.Top, Height=20)
        let panelLeft = new Panel(Dock=DockStyle.Left, Width=200)

        panelFilter.Controls.AddRange [|
                txtFilter
                new Label(Text="Word filter:", Dock=DockStyle.Left)
            |]
        panelListSize.Controls.AddRange [|
                txtListSize
                new Label(Text="List size:", Dock=DockStyle.Left)
            |]

        panelLeft.Controls.AddRange [| panelListSize; panelFilter |]
        form.Controls.AddRange [| panelLeft; listboxOutput |]
        
    do 
        formLayout ()
        txtFilter.Text <- initialSettings.filter |> Strings.joinWords
        txtListSize.Text <- initialSettings.listSize.ToString()

        stateUpdateEvents.Add (controlInvoke form displayState)

    member this.Show() =
        do Application.EnableVisualStyles()
        do Application.Run(form)

    member this.Closed = form.FormClosed |> Observable.map ignore
    member this.Shown = form.Shown |> Observable.map ignore