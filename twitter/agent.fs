﻿module agent

open System
open System.Threading
open FSharp.Control

/// Agent that receives messages and emits them using the MessageReceived event
type ObservableAgent<'T>()=
    let event = new Event<'T>()
    
    let cts = new CancellationTokenSource()

    let body (agent:Agent<'T>) =
        let rec loop () =
            async { let! msg = agent.Receive()
                    do event.Trigger(msg)
                    return! loop() }
        loop ()

    let agent = Agent.Start(body, cts.Token) 
    
    [<CLIEvent>]
    member x.MessageReceived = event.Publish

    member x.Post = agent.Post
    
    interface IDisposable with
        member x.Dispose() = cts.Cancel()