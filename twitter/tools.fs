﻿module tools

open System.IO
open System.Windows.Forms
open TweetinCore.Interfaces

type Delegate<'t> = delegate of 't -> unit
let controlInvoke (control:Control) f (param:'t) =
    control.Invoke(new Delegate<'t>(f), param) |> ignore

let tweetToStr (tweet:ITweet) =
    sprintf "Creator:  %s\nTime:  %s\nText:  %s" 
            tweet.Creator.ScreenName 
            (tweet.CreatedAt.ToLongTimeString())
            tweet.Text

let tweetFileWriter outputFile = 
    let fileWriter (output) =
        let out = File.CreateText(output)
        fprintfn out "%s"

    tweetToStr >> fileWriter outputFile