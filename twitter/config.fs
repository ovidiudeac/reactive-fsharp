﻿module oauth

open System.Configuration

module private impl =
    let read configFile =
        let configMap = new ExeConfigurationFileMap(ExeConfigFilename = configFile)
        let config = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
        fun key -> (config.AppSettings.Settings.Item key).Value

exception ParseError

type Config = 
    {
        appKey : string
        appSecret : string
        accessToken : string
        accessTokenSecret : string
    }

let parse (configFile:string) = 
    let read = impl.read configFile
    { 
        appKey = read "appKey"
        appSecret = read "appSecret"
        accessToken = read "accessToken"
        accessTokenSecret = read "accessTokenSecret"
    }