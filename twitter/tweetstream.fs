﻿module tweetstream

open System
open System.Threading.Tasks

open Streaminvi
open TweetinCore.Interfaces 
open FSharp.Control
open FSharpx
open FSharpx.Stm
open FSharp.Reactive
open model
open agent

type TweetStream ( cfg:oauth.Config, 
                   streamUrl: string) as this =
    let token = TwitterToken.Token (cfg.accessToken,
                                    cfg.accessTokenSecret,
                                    cfg.appKey, 
                                    cfg.appSecret)

    let stream = new SimpleStream(streamUrl)
    let agent = new ObservableAgent<ITweet>()    

    member this.TweetReceived = agent.MessageReceived
    member this.Started = stream.StreamStarted |> Observable.map ignore

    member this.Start() = 
        Task.Factory.StartNew (fun _ -> stream.StartStream(token, agent.Post))
        |> ignore        

    member this.Stop() = stream.StopStream ()
