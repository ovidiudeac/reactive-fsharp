﻿module model
open TweetinCore.Interfaces

type UserInput = 
    | ListSizeChanged of int
    | FilterChanged of string list

type AppInput = 
    | TweetReceived of ITweet 
    | UserAction of UserInput

type TweetState =
    | Starting
    | Running of ITweet list

type Settings = 
    {
        filter : string list
        listSize : int
    }

type State =
    {
        tweets : ITweet list
        settings : Settings
    }