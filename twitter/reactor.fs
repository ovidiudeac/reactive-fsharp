﻿module reactor

open System
open FSharpx.Collections
open FSharpx
open FSharp.Reactive
open TweetinCore.Interfaces

open model
open agent

let filterTweets (settings:Settings) = 
    let filterWords (tweet:ITweet) =
        let tweetWords = 
            tweet.Text.ToLowerInvariant() 
            |> Strings.split ' '

        let tweetHasWord word =
            tweetWords |> Seq.exists (fun w -> w = word)

        let tweetContainsAnyFilterWords = 
            settings.filter
            |> Seq.exists tweetHasWord

        settings.filter.IsEmpty || tweetContainsAnyFilterWords 
            

    Seq.filter filterWords
    >> Seq.truncate settings.listSize
    >> List.ofSeq
    
let updateSettings previousSettings userAction =
    match userAction with
    | ListSizeChanged newSize -> 
        { previousSettings with
            listSize = newSize
        }

    | FilterChanged newFilter ->
        { previousSettings with
            filter = newFilter
        }
            
let transition previousState input = 
    match input with
    | UserAction userAction ->
        let newSettings = updateSettings previousState.settings userAction
        { previousState with
            settings = newSettings
            tweets = filterTweets newSettings previousState.tweets
        }

    | TweetReceived tweet ->
        let newList = 
            List.cons tweet previousState.tweets
            |> filterTweets previousState.settings

        { previousState with
            tweets = newList
        }

let main (initialSettings:Settings) 
         (incomingTweets: ITweet IObservable) 
         (uiActions: UserInput IObservable) :  State IObservable = 
    let initialState = { settings = initialSettings; tweets = [] }

    let tweetInput = Observable.map TweetReceived incomingTweets
    let userInput = Observable.map UserAction uiActions
    let allInput = Observable.merge userInput tweetInput

    Observable.scan transition initialState allInput
