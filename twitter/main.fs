﻿open System

open tweetstream
open view
open model 
open tools


[<EntryPoint>]
let main argv = 
    try
        let oauthCfg = oauth.parse @"..\..\oauth.config"

        
        let stream = new TweetStream (oauthCfg, 
                                      "https://stream.twitter.com/1.1/statuses/sample.json")            
 
        stream.TweetReceived.Add (tweetFileWriter "tweet.out")
        
        let initialSettings = 
            {
                filter = []; 
                listSize = 50; 
            }
        
        let reactor = reactor.main initialSettings stream.TweetReceived

        let form = new MyForm(initialSettings, reactor)

        form.Shown.Add stream.Start
        form.Closed.Add stream.Stop

        form.Show()
    
        0
            
    with ex -> 
        printfn "Exception: %s" ex.Message
        printf "Exit."
        System.Console.ReadKey() |> ignore
        1
